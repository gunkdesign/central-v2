# Central Readme.md file


## Summary

This repository is a whitelabel for GunkDesign Projects

## Dependencies

This project requires:

+ [HTML5 Boilerplate](http://html5boilerplate.com)
+ [SASS](http://sass-lang.com)
+ [Bourbon](http://bourbon.io)
+ [Neat](http://neat.bourbon.io)
+ [Bitters](https://github.com/thoughtbot/bitters)

## SASS Main Folder Structure

+ ** Normalize ** - Resets File
+ ** Bitters ** - Base reset of common elements
+ ** Bourbon ** - Main Mixin Library
+ ** Neat ** - Grid files
+ ** Theme ** - Contains styles specific to this sites layout e.g. like a child theme
+ ** Central ** - Styles that are generally used like a custom bitters

### The Way It Works

The **Thoughtbot modules** are the backbone of the site providing the grid and mixins. **Central** provides a custom collection of styles that can be dropped in like modules. Finally, **Theme** are the styles specific to this site.


## CSS Output Files

+ ** Screen ** - Main output file
+ ** Print ** - Style for print

## Kit/HTML

Based on HTML5 Boiler plate but using Codekit's Kit template language to make things includes. Each **Central** Kit partial matches to the **Central** SASS mixin library.

