$(document).ready(function() {
	$('body').addClass('js');
	var $menu = $('.main-navigation'),
	$menulink = $('.main-navigation-link');
	
	$menulink.click(function() {
		$menulink.toggleClass('active');
		$menu.toggleClass('active');
		event.preventDefault();
	});

});